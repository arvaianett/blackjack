import { AppComponent } from './app.component';
import { deck } from './deck';

describe('AppComponent', () => {
  let appComponent: AppComponent = new AppComponent();

  it(`should be less then the deck's length`, () => {
    expect(appComponent.getRandomNumberMaxDeck(deck)).toBeLessThanOrEqual(deck.length);
  });

  it('should be print sam as a winner when he has 21 from 2 cards', () => {
    const samsScore = 21;
    const dealersScore = 12;
    const samsCards = [];
    const dealersCards = [];
    expect(appComponent.getWinner(samsScore, dealersScore, samsCards, dealersCards, deck)).toBe('sam');
  });

  it('should print dealer as a winner when he has 21 from 2 cards', () => {
    const samsScore = 5;
    const dealersScore = 21;
    const samsCards = [];
    const dealersCards = [];
    expect(appComponent.getWinner(samsScore, dealersScore, samsCards, dealersCards, deck)).toBe('dealer');
  });

  it('should print sam as a winner when both have 21 from 2 cards', () => {
    const samsScore = 21;
    const dealersScore = 21;
    const samsCards = [];
    const dealersCards = [];
    expect(appComponent.getWinner(samsScore, dealersScore, samsCards, dealersCards, deck)).toBe('sam');
  });

  it('should print dealer as a winner when both have 22 from 2 cards', () => {
    const samsScore = 22;
    const dealersScore = 22;
    const samsCards = [];
    const dealersCards = [];
    expect(appComponent.getWinner(samsScore, dealersScore, samsCards, dealersCards, deck)).toBe('dealer');
  });

  it('should return 21 as score', () => {
    const cards = ['CA', 'CK'];
    expect(appComponent.getScore(cards)).toBe(21);
  });

  it('should return 15 as score', () => {
    const cards = ['C5', 'CK'];
    expect(appComponent.getScore(cards)).toBe(15);
  });

  it('should remove the first item of an array', () => {
    const array = ['C5', 'C6', 'C7'];
    expect(appComponent.removeDrawnCard(array)).length === 2;
  });

  it('should get the first item of an array', () => {
    const array = ['C5', 'C6', 'C7'];
    expect(appComponent.draw(array)).toEqual('C5');
  });
});
