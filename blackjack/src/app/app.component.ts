import { Component, OnInit } from '@angular/core';
import { deck } from './deck';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  deck: string[];
  samsCards: string[];
  dealersCards: string[];
  samsScore: number;
  dealersScore: number;

  constructor() {
    this.samsCards = [];
    this.dealersCards = [];
    this.samsScore = 0;
    this.dealersScore = 0;
  }

  ngOnInit() {
    this.deck = deck;
    this.shuffleDeck(this.deck);
    for (let i = 0; i < 4; i++) {
      if (i % 2 === 0) {
        this.samsCards.push(this.draw(this.deck));
      } else {
        this.dealersCards.push(this.draw(this.deck));
      }
      this.deck = this.removeDrawnCard(this.deck);
    }
    this.samsScore = this.getScore(this.samsCards);
    this.dealersScore = this.getScore(this.dealersCards);
    const winner = this.getWinner(this.samsScore, this.dealersScore, this.samsCards, this.dealersCards, this.deck);
    this.printTheResult(winner);
  }

  shuffleDeck(deck: string[]): string[] {
    let currentIndex: number = deck.length;
	  let temporaryValue: string, randomIndex: number;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      temporaryValue = deck[currentIndex];
      deck[currentIndex] = deck[randomIndex];
      deck[randomIndex] = temporaryValue;
    }
    return deck;
  }

  getWinner(samsScore: number, dealersScore: number, samsCards: string[], dealersCards: string[], deck: string[]): string {
    let winner: string;
    if (samsScore === 21 && dealersScore === 21 || samsScore === 21 && dealersScore < 21) {
      winner = 'sam';
    } else if (samsScore < 21 && dealersScore === 21 || samsScore === 22 && dealersScore === 22) {
      winner = 'dealer';
    } else {
      while (samsScore <= 17) {
        samsCards.push(this.draw(deck));
        deck = this.removeDrawnCard(deck);
        samsScore = this.getScore(samsCards);
      }
      if (samsScore > 21) {
        winner = 'dealer';
      } else {
        while (samsScore > dealersScore) {
          dealersCards.push(this.draw(deck));
          deck = this.removeDrawnCard(deck);
          dealersScore = this.getScore(dealersCards);
        }
        if (samsScore === dealersScore) {
          winner = 'tie';
        } else if (dealersScore > 21) {
          winner = 'sam';
        } else {
          winner = 'dealer';
        }
      }
    }
    return winner;
  }

  getScore(cards: string[]): number {
    let score: number = 0;
    const score10 = ['10', 'J', 'Q', 'K'];
    cards.forEach(card => {
      if (card.substring(1) === 'A') {
        score = score + 11;
      } else if (score10.includes(card.substring(1))) {
        score = score + 10;
      } else {
        score = score + Number(card.substring(1));
      }
    });
    return score;
  }

  draw(deck: string[]): string {
    return deck[0];;
  }

  removeDrawnCard(deck: string[]): string[] {
    return deck.filter((c, i) => i !== 0);
  }

  getRandomNumberMaxDeck(deck: string[]): number {
    return Math.floor(Math.random() * Math.floor(deck.length));
  }

  printTheResult(winner: string): void {
    console.log(winner);
    console.log('sam:', this.samsCards, this.samsScore);
    console.log('dealer:', this.dealersCards, this.dealersScore);
  }
}
